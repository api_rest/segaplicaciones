<?php

namespace App\Http\Middleware;

use Closure;
use Auth0\SDK\JWTVerifier;

class CheckScopes
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */


    public function handle($request, Closure $next, $scope)
    {
        if(!$request->hasHeader('Authorization')) {
          return response()->json('Authorization Header not found', 401);
        }

        $token = $request->bearerToken();
        $this->token = $token;

        if($request->header('Authorization') == null || $token == null) {
          return response()->json('No token provided', 401);
        }

        try {
            $verifier = new JWTVerifier([
              'supported_algs' => ['RS256'],
              'valid_audiences' => ['http://seg.aplicaciones/api'],
              'authorized_iss' => ['https://lucasag.auth0.com/']
            ]);

            $decoded = $verifier->verifyAndDecode($token);

            if($scope) {
                $hasScope = false;
                if(isset($decoded->scope)) {
                    $scopes = explode(" ", $decoded->scope);
                    foreach ($scopes as $s) {
                        if ($s === $scope)
                            $hasScope = true;
                    }
                }
                if(!$hasScope)
                    return response()->json(["message" => "Insufficient scope"], 403);
            }
        }
        catch(\Auth0\SDK\Exception\CoreException $e){
            throw $e;
        }

        return $next($request);
    }

}