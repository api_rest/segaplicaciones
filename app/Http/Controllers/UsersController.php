<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;
use App\User;

class UsersController extends Controller
{
    function index(Request $request){
        if ($request->isJson()){
            $users = User::all();
            return response()->json($users, 200);
        }
        return response()->json(['error' => 'Unauthorized'], 401);
    }
    function unUsuario($id){
        $user = User::find($id);
        if ($user){
            return response()->json($user, 200);
        }
        else{
            return response()->json(['error' => 'User not found'], 404);
        }
    }

    function crearUser(Request $request){
        if ($request->isJson()){
            $data = $request->json()->all();
            $user = User::create([
                'username' => $data['username'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'api_token' => str_random(60),
            ]);
            return response()->json($user, 201);
        }
        return response()->json(['error' => 'Unauthorized'], 401);
    }

    function updateUser($id, Request $request){
        try{
            if($request->isJson()){
                $data = $request->json()->all();
                $user = User::findOrFail($id);
                if (isset($data['username']))
                    $user->username = $data['username'];
                if (isset($data['email']))
                    $user->email = $data['email'];
                if (isset($data['password']))
                    $user->password = Hash::make($data['password']);
                $user->save();
                return response()->json(['status' => 'Updated Successfully'], 200);
            }
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        catch(ModelNotFoundException $e){
            return response()->json(['error' => 'No content'], 406);
        }
    }

    function eliminarUser($id, Request $request){
        try{
            if($request->isJson()){
                $user = User::findOrFail($id)->delete();
                return response()->json(['status' => 'Deleted Successfully'], 200);
            }
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        catch(ModelNotFoundException $e){
            return response()->json(['error' => 'No content'], 406);
        }
    }

    function postUser(Request $request){
        if ($request->isJson()){
            try{
                $data = $request->json()->all();
                $user = User::where('username', $data['username'])->first();
                $token = $user->api_token;
                if ($user && Hash::check($data['password'], $user->password)){
                    return response()->json($user, 200);
                }
                else{
                    return response()->json(['error' => 'Not matched'], 406);
                }
            }
            catch(ModelNotFoundException $e){
                return response()->json(['error' => 'No content'], 406);
            }
        }
        return response()->json(['error' => 'Unauthorized'], 401);
    }
}