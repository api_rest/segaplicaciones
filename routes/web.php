<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->group(['middleware' => ['auth']], function() use ($router){
        $router->get('users', ['middleware' => 'check.scopes:read:users', 'uses' => 'UsersController@index']);
        $router->get('users/{id}', ['middleware' => 'check.scopes:read:users','uses' => 'UsersController@unUsuario']);
        $router->post('users', ['middleware' => 'check.scopes:create:users','uses' => 'UsersController@crearUser']);
        $router->post('users/{id}', ['middleware' => 'check.scopes:update:users','uses' => 'UsersController@updateUser']);
        $router->delete('users/{id}', ['middleware' => 'check.scopes:delete:users','uses' => 'UsersController@eliminarUser']);
    });
});